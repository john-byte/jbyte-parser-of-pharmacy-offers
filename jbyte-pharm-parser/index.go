package jbytepharmparser

import (
	trie "jbyte-parser-of-pharmacy-offers/trie"
	"jbyte-parser-of-pharmacy-offers/types"
	"strconv"
	"strings"
)

// [VERIFIED-SOLUTION-OK]
type TJbytePharmParser struct {
	startCodes       *trie.Trie
	unitMeasureUnits *trie.Trie
	quantityUnits    *trie.Trie
	priceUnits       *trie.Trie
}

func New() *TJbytePharmParser {
	startCodes := trie.New()
	startCodes.PutKey([]rune("в наличии"))
	startCodes.PutKey([]rune("в наличие"))
	startCodes.PutKey([]rune("оригинал"))
	startCodes.PutKey([]rune("оптом"))
	startCodes.PutKey([]rune("опт"))
	startCodes.PutKey([]rune("оптовый"))
	startCodes.PutKey([]rune("склад"))

	unitMeasureUnits := trie.New()
	unitMeasureUnits.PutKey([]rune("мг"))
	unitMeasureUnits.PutKey([]rune("г"))
	unitMeasureUnits.PutKey([]rune("mg"))
	unitMeasureUnits.PutKey([]rune("g"))
	unitMeasureUnits.PutKey([]rune("мл"))
	unitMeasureUnits.PutKey([]rune("л"))
	unitMeasureUnits.PutKey([]rune("ml"))
	unitMeasureUnits.PutKey([]rune("l"))

	quantityUnits := trie.New()
	quantityUnits.PutKey([]rune("шт"))
	quantityUnits.PutKey([]rune("табл"))
	quantityUnits.PutKey([]rune("таб"))

	priceUnits := trie.New()
	priceUnits.PutKey([]rune("сом"))
	priceUnits.PutKey([]rune("$"))

	return &TJbytePharmParser{
		startCodes:       &startCodes,
		unitMeasureUnits: &unitMeasureUnits,
		quantityUnits:    &quantityUnits,
		priceUnits:       &priceUnits,
	}
}

func isNameChar(ch rune) bool {
	return !('0' <= ch && ch <= '9') && ch != '-' && ch != '–' && ch != '—' &&
		ch != '(' && ch != ')' && ch != '\n'
}

func isDigit(ch rune) bool {
	return '0' <= ch && ch <= '9'
}

func (self *TJbytePharmParser) ParseMessage(text string) []types.TExternalOffer {
	loweredText := strings.ToLower(text)
	textIter := []rune(loweredText)
	ptr := 0

	// 1. Check start code & skip until next lines
	hasStartCode := false
	for ptr < len(textIter) && !hasStartCode {
		if self.startCodes.GetEndOfPrefix(textIter[ptr:]) >= 0 {
			hasStartCode = true
		}
		ptr++
	}
	if !hasStartCode {
		return nil
	}
	ptr++

	// 3. Process each line yay :3
	res := make([]types.TExternalOffer, 0)
	for ptr < len(textIter) {
		nameBuff := strings.Builder{}
		numBuff := strings.Builder{}

		unitMeasureUnits := ""
		unitMeasure := 0.0
		quantityUnits := ""
		quantity := 0
		priceUnits := ""
		price := 0.0

		for ptr < len(textIter) && textIter[ptr] != '\n' {
			if len(unitMeasureUnits) == 0 && len(quantityUnits) == 0 && len(priceUnits) == 0 &&
				numBuff.Len() == 0 && isNameChar(textIter[ptr]) {
				for ptr < len(textIter) && isNameChar(textIter[ptr]) {
					nameBuff.WriteRune(textIter[ptr])
					ptr++
				}
				continue
			}

			if len(unitMeasureUnits) == 0 {
				unitMeasureUnitsEnd := self.unitMeasureUnits.GetEndOfPrefix(textIter[ptr:])
				if unitMeasureUnitsEnd >= 0 {
					unitMeasureUnits = string(textIter[ptr : ptr+unitMeasureUnitsEnd])
					if numBuff.Len() > 0 {
						numStr := numBuff.String()
						numBuff = strings.Builder{}
						unitMeasure, _ = strconv.ParseFloat(numStr, 64)
					}

					ptr += len(unitMeasureUnits)
					continue
				}
			}

			if len(quantityUnits) == 0 {
				quantityUnitsEnd := self.quantityUnits.GetEndOfPrefix(textIter[ptr:])
				if quantityUnitsEnd >= 0 {
					quantityUnits = string(textIter[ptr : ptr+quantityUnitsEnd])
					if numBuff.Len() > 0 {
						numStr := numBuff.String()
						numBuff = strings.Builder{}
						num, _ := strconv.ParseFloat(numStr, 64)
						quantity = int(num)
					}

					ptr += len(quantityUnits)
					continue
				}
			}

			if len(priceUnits) == 0 {
				priceUnitsEnd := self.priceUnits.GetEndOfPrefix(textIter[ptr:])
				if priceUnitsEnd >= 0 {
					priceUnits = string(textIter[ptr : ptr+priceUnitsEnd])
					if numBuff.Len() > 0 {
						numStr := numBuff.String()
						numBuff = strings.Builder{}
						price, _ = strconv.ParseFloat(numStr, 64)
					}

					ptr += len(priceUnits)
					continue
				}
			}

			if (len(unitMeasureUnits) == 0 || len(quantityUnits) == 0 || len(priceUnits) == 0) &&
				isDigit(textIter[ptr]) {
				for ptr < len(textIter) && isDigit(textIter[ptr]) {
					numBuff.WriteRune(textIter[ptr])
					ptr++
				}

				if ptr < len(textIter) && (textIter[ptr] == '.' || textIter[ptr] == ',') {
					numBuff.WriteRune('.')
					ptr++

					for ptr < len(textIter) && isDigit(textIter[ptr]) {
						numBuff.WriteRune(textIter[ptr])
						ptr++
					}
				} else {
					numBuff.WriteString(".0")
				}
				continue
			}

			ptr++
		}
		ptr++

		name := strings.Trim(nameBuff.String(), " ")
		if len(name) > 0 && (len(unitMeasureUnits) > 0 || len(quantityUnits) > 0 || len(priceUnits) > 0) {
			externalOffer := types.TExternalOffer{
				Name:                 name,
				UnitMeasureUnits:     unitMeasureUnits,
				UnitMeasure:          unitMeasure,
				QuantityMeasureUnits: quantityUnits,
				QuantityMeasure:      uint64(quantity),
				PriceUnits:           priceUnits,
				Price:                price,
			}
			res = append(res, externalOffer)
		}
	}

	return res
}

func (self *TJbytePharmParser) ParsePhoneNumberFromMessage(text string) string {
	textIter := []rune(text)

	ptr := 0
	for ptr < len(textIter) {
		if isDigit(textIter[ptr]) || textIter[ptr] == '+' {
			numBuff := strings.Builder{}

			for ptr < len(textIter) && (isDigit(textIter[ptr]) || textIter[ptr] == '+') {
				numBuff.WriteRune(textIter[ptr])
				ptr++
			}

			numNormalized := strings.Trim(
				numBuff.String(),
				" ",
			)
			if len(numNormalized) >= 9 {
				return numNormalized
			}
			continue
		}
		ptr++
	}

	return ""
}
