package types

type TExternalOffer struct {
	Name                 string  `json:"name" bson:"Name"`
	UnitMeasureUnits     string  `json:"unitMeasureUnits" bson:"UnitMeasureUnits"`
	UnitMeasure          float64 `json:"unitMeasure" bson:"UnitMeasure"`
	QuantityMeasureUnits string  `json:"quantityMeasureUnits" bson:"QuantityMeasureUnits"`
	QuantityMeasure      uint64  `json:"quantityMeasure" bson:"QuantityMeasure"`
	PriceUnits           string  `json:"priceUnits" bson:"PriceUnits"`
	Price                float64 `json:"price" bson:"Price"`
	CreatedAt            string  `json:"createdAt" bson:"CreatedAt"`
	CreatedBy            string  `json:"createdBy" bson:"CreatedBy"`
	ChatNick             string  `json:"chatNick" bson:"ChatNick"`
	CreatedByPhone       string  `json:"createdByPhone" bson:"CreatedByPhone"`
}
